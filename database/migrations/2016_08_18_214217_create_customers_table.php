<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('title');
            $table->string('fname');
            $table->string('lname');
            $table->string('gender');
            $table->string('street');
            $table->string('city');
            $table->string('state');
            $table->string('postcode');
            $table->date('dob');
            $table->string('phone');
            $table->string('cell');
            $table->string('id_name');
            $table->string('id_value');            
            $table->string('img_large');
            $table->string('img_med');
            $table->string('img_thumb');
            $table->string('nat', 4);
            $table->timestamps();
        });

        // Schema::create('customers_details', function (Blueprint $table) {
        //     $table->integer('customer_id')->unsigned();
        //     $table->string('gender');
        //     $table->string('street');
        //     $table->string('city');
        //     $table->string('state');
        //     $table->string('postcode');
        //     $table->date('dob');
        //     $table->string('phone');
        //     $table->string('cell');
        //     $table->string('img_large');
        //     $table->string('img_med');
        //     $table->string('img_thumb');
        //     $table->string('nat', 4);
        //     $table->timestamps();
        //     //fkey
        //     $table->foreign('customer_id')
        //           ->references('id')
        //           ->on('customers')
        //           ->onDelete('cascade');
        //     //pkey
        //     $table->primary(['customer_id']);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
        // Schema::drop('customers_details');
    }
}
