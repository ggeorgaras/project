<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'username', 'email' , 'password' ,
  		'title' ,'fname' ,'lname' ,'gender' ,
  		'street' ,'city' ,'state' ,'postcode' ,
  		'dob' ,'phone' ,'cell' ,'id_name', 
  		'id_value' , 'img_large' , 'img_med' , 
  		'img_thumb' , 'nat'
    ];
    
}


