<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    // A role my have many permissions.
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    
    // Grant the given permission to a role.
    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }
}