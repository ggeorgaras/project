<?php

namespace App;

trait HasRoles
{

    //User can have multiple roles. 
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    //Check the role that any user have 
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        foreach ($role as $urole) {
        	if ($this->hasRole($urole->name)) {
        		return true;
        	}
        }

        return false;

    }

    // Give any role to user.
    public function giveRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }


}