<?php

namespace App\Http\Controllers;

use Gate;
use Auth;
use App\Customer;
use App\Http\Requests;
use Illuminate\Http\Request;

class UserListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate(15);

        // return view('customers.index', [
        //             'permissions' => Auth::user()->hasRole('super-admin'),
        //             'customers' => Customer::all()
        //             ]);

        return view('customers.index', compact('customers'));
    }
 
}
