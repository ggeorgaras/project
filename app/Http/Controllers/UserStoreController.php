<?php

namespace App\Http\Controllers;

use File;
use Carbon\Carbon;
use App\Customer;
use App\Http\Requests;
use Illuminate\Http\Request;

class UserStoreController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $data = json_decode($request->getContent());

        foreach ($data as $obj) {

			$customer_data = array(
				'username' => $obj->login->username,
				'email' => $obj->email,
				'password' => bcrypt($obj->login->password),
				'title' => $obj->name->title,
				'fname' => $obj->name->first,
				'lname' => $obj->name->last,
				'gender' => $obj->gender,
				'street' => $obj->location->street,
				'city' => $obj->location->city,
				'state' => $obj->location->state,
				'postcode' => $obj->location->postcode,
				'dob' => $obj->dob,
				'phone' => $obj->phone,
				'cell' => $obj->cell,
				'id_name' => $obj->id->name,
				'id_value' => $obj->id->value,
				'img_large' => $obj->picture->large,
				'img_med' => $obj->picture->medium,
				'img_thumb' => $obj->picture->thumbnail,
				'nat' => $obj->nat
			);

        	// Create or update data based on the customer email
        	Customer::updateOrCreate(['email' => $obj->email], $customer_data);

        }
        return response()->json(['response' => "Task completed."],200);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

 
}
