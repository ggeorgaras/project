<?php

Route::auth();

Route::get('/', 'HomeController@index');

Route::get('customers', 'UserListController@index');
Route::get('customers/{customer}', 'UserViewController@show');

Route::delete('customers/{customer}/delete', 'UserDeleteController@destroy')->name('delete');

Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function () {

	Route::resource('/addTask', 'UserStoreController@create');

	

});