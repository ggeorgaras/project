@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (count($customers) > 1)
            <table class="table table-bordered table-striped table-hover" data-toggle="dataTable" data-form="deleteForm">
                <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Nationality</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>           
                @foreach($customers as $customer)
                    <tr>
                        <td class="text-center"><img src="{{ $customer->img_thumb }}" alt="{ $customer->fname }} {{ $customer->lname }}" class="img-circle"></td>
                        <td>{{ studly_case($customer->fname) }} {{ studly_case($customer->lname) }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->nat }}</td>
                        <td>
                            <a href="/customers/{{ $customer->id }}" class="btn btn-info btn-xs pull-right">View</a>
                            
                            @if (Auth::user()->hasRole('super-admin'))
                                <form class="delete pull-right" action="{{ route('delete', $customer->id) }}" method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-danger btn-xs" value="Delete">
                                </form>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row row-centered">
                {{ $customers->links() }}
            </div>
        @else
            <div class="alert alert-info" role="alert"> 
                <strong>Heads up!</strong> No customers data found!!! 
            </div>
        @endif

        </div>
    </div>
</div>

@endsection