@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-4">
            


    <div class="card hovercard">
        <div class="card-background">
            <img class="card-bkimg" alt="" src="{{ $customer->img_thumb }}">
            <!-- http://lorempixel.com/850/280/people/9/ -->
        </div>
        <div class="useravatar">
            <img alt="" src="{{ $customer->img_large }}">
        </div>
        <div class="card-info"> <span class="card-title">{{ studly_case($customer->fname) }} {{ studly_case($customer->lname) }}</span>

        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">About</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                <div class="hidden-xs">Location</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>
                <div class="hidden-xs">Phones</div>
            </button>
        </div>
    </div>

        <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
            <address>
                <strong>Contact Details</strong><br>
                {{ $customer->street }}<br>
                {{ studly_case($customer->city) }}, {{ studly_case($customer->state) }} {{ $customer->postcode }}<br>
                <abbr title="Phone">P:</abbr> {{ $customer->phone }}<br>
                <abbr title="Mob">M:</abbr> {{ $customer->cell }}
            </address>
            <address>
              <a href="mailto:#">{{ $customer->email }}</a>
            </address>
        </div>
        <div class="tab-pane fade in" id="tab2">
            <address>
                <strong>Contact Details</strong><br>
                {{ $customer->street }}<br>
                {{ studly_case($customer->city) }}, {{ studly_case($customer->state) }} {{ $customer->postcode }}<br>
                <abbr title="Phone">P:</abbr> {{ $customer->phone }}<br>
                <abbr title="Mob">M:</abbr> {{ $customer->cell }}
            </address>
            <address>
              <a href="mailto:#">{{ $customer->email }}</a>
            </address>
        </div>
        <div class="tab-pane fade in" id="tab3">
          <h3>This is tab 3</h3>
        </div>
      </div>
    </div>
    
            
    



        </div>

    </div>
</div>



@endsection