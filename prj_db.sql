-- MySQL dump 10.13  Distrib 5.7.10, for osx10.11 (x86_64)
--
-- Host: localhost    Database: prj_db
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cell` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_large` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_med` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nat` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_username_unique` (`username`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (2,'orangebear890','viggo.hemelaar@example.com','$2y$10$ClCFVbMbkxqr0cbadItJ2OFj9A3bGWzKQ7bWQMp/XhoV1pobBz3dy','mr','viggo','hemelaar','male','5199 jan pieterszoon coenstraat','borsele','groningen','88570','0000-00-00','(058)-571-6511','(231)-590-8223','BSN','16162369','https://randomuser.me/api/portraits/men/90.jpg','https://randomuser.me/api/portraits/med/men/90.jpg','https://randomuser.me/api/portraits/thumb/men/90.jpg','NL','2016-08-18 22:29:43','2016-08-19 01:38:23'),(3,'ticklishcat637','adrian.may@example.com','$2y$10$9XyOcHtQNE63NR9rFGAZhOvuRFX2GNOlZhbeJidOyfa37P81oui2e','mr','adrian','may','male','7518 hamilton ave','temecula','massachusetts','34197','0000-00-00','(363)-691-0507','(038)-565-5241','SSN','446-67-4913','https://randomuser.me/api/portraits/men/38.jpg','https://randomuser.me/api/portraits/med/men/38.jpg','https://randomuser.me/api/portraits/thumb/men/38.jpg','US','2016-08-18 22:30:20','2016-08-19 01:38:23'),(4,'bluerabbit720','marina.vega@example.com','$2y$10$DkfJ6qumk8jiD2vRbLyKfuzGubuQvaF3hcASpG1Me1BOU3LYI0azW','miss','marina','vega','female','3377 paseo de extremadura','fuenlabrada','galicia','51749','0000-00-00','983-222-035','686-274-708','DNI','20631425-P','https://randomuser.me/api/portraits/women/90.jpg','https://randomuser.me/api/portraits/med/women/90.jpg','https://randomuser.me/api/portraits/thumb/women/90.jpg','ES','2016-08-18 22:30:20','2016-08-19 01:38:23'),(5,'redbutterfly772','freja.olsen@example.com','$2y$10$v//j/dRSyOyJDZJ3k96X8.1a4.LG.DZ5cfz7jZYfgFxIhwcxra.x.','miss','freja','olsen','female','6325 skagensvej','agerbæk','nordjylland','41661','0000-00-00','49450289','41279503','CPR','356541-6231','https://randomuser.me/api/portraits/women/13.jpg','https://randomuser.me/api/portraits/med/women/13.jpg','https://randomuser.me/api/portraits/thumb/women/13.jpg','DK','2016-08-18 22:30:21','2016-08-19 01:38:23'),(6,'beautifulduck157','denny.potter@example.com','$2y$10$aCgr0o8WZXw8Ff6Z4DjxmO3ZeqpvR566ZBX/zgHD95Buz36N0BshS','mr','denny','potter','male','3679 springweg','nunspeet','friesland','20335','0000-00-00','(060)-576-0965','(537)-031-4696','BSN','18463501','https://randomuser.me/api/portraits/men/36.jpg','https://randomuser.me/api/portraits/med/men/36.jpg','https://randomuser.me/api/portraits/thumb/men/36.jpg','NL','2016-08-18 22:30:21','2016-08-19 01:38:23'),(7,'silverrabbit646','tommy.harper@example.com','$2y$10$8T4K86It.vBRQ7Levtsh0uNpK.Vknwb5gDl4RWJq/3Wd3Woe5FiaO','mr','tommy','harper','male','7174 crockett st','adelaide','new south wales','1162','0000-00-00','09-4103-3856','0483-381-656','TFN','183336754','https://randomuser.me/api/portraits/men/28.jpg','https://randomuser.me/api/portraits/med/men/28.jpg','https://randomuser.me/api/portraits/thumb/men/28.jpg','AU','2016-08-18 22:30:21','2016-08-19 01:38:23'),(8,'blackbird571','louise.dubois@example.com','$2y$10$LuapFtS61ZKD2NlYiflLZuDCKgencisMeBTML7OyeE4cyjy1wceuq','mrs','louise','dubois','female','6619 rue du cardinal-gerlier','toulouse','mayenne','40499','0000-00-00','01-65-63-45-24','06-59-31-60-82','INSEE','284360158451 21','https://randomuser.me/api/portraits/women/56.jpg','https://randomuser.me/api/portraits/med/women/56.jpg','https://randomuser.me/api/portraits/thumb/women/56.jpg','FR','2016-08-18 22:30:21','2016-08-19 01:38:23'),(9,'bigostrich807','ethan.williamson@example.com','$2y$10$y53e4N0/aws9FZqxTdGMSOoz7AWNulhmVsXxlXJvkbXcKn9lMcO5S','mr','ethan','williamson','male','4050 cackson st','pembroke pines','idaho','32790','0000-00-00','(867)-895-6975','(982)-701-5328','SSN','967-50-1766','https://randomuser.me/api/portraits/men/83.jpg','https://randomuser.me/api/portraits/med/men/83.jpg','https://randomuser.me/api/portraits/thumb/men/83.jpg','US','2016-08-18 22:30:21','2016-08-19 01:38:23'),(10,'silverbutterfly237','milla.ollila@example.com','$2y$10$MhQ5mpAL03z3kcI8HBb6Iu0NIm1zRNZ3ZyjjJIhffrbJtyIJhe4h2','mrs','milla','ollila','female','7754 hämeenkatu','ypäjä','päijät-häme','25760','0000-00-00','05-682-831','043-823-89-73','HETU','79254567-S','https://randomuser.me/api/portraits/women/81.jpg','https://randomuser.me/api/portraits/med/women/81.jpg','https://randomuser.me/api/portraits/thumb/women/81.jpg','FI','2016-08-18 22:30:21','2016-08-19 01:38:23'),(11,'biggorilla772','anton.ernst@example.com','$2y$10$JHTfj2GicvmHoQyvNi5BjeAigpzh9DDIvCjEhd7txjPkAg2PXKAYO','mr','anton','ernst','male','5416 industriestraße','rhön-grabfeld','baden-württemberg','70524','0000-00-00','0268-5798589','0179-0278223','','','https://randomuser.me/api/portraits/men/16.jpg','https://randomuser.me/api/portraits/med/men/16.jpg','https://randomuser.me/api/portraits/thumb/men/16.jpg','DE','2016-08-18 22:30:21','2016-08-19 01:38:24'),(12,'bluemouse870','nurdan.çamdalı@example.com','$2y$10$qg7sstXAYoI8TClRvmaKRO1MAJiU3Kf8M8QCJVZqGaYSVRH2eV6UK','ms','nurdan','çamdalı','female','9866 maçka cd','iğdır','balıkesir','81367','0000-00-00','(554)-419-8129','(752)-983-4127','','','https://randomuser.me/api/portraits/women/2.jpg','https://randomuser.me/api/portraits/med/women/2.jpg','https://randomuser.me/api/portraits/thumb/women/2.jpg','TR','2016-08-18 22:30:21','2016-08-19 01:38:24'),(13,'yellowrabbit392','oseias.silveira@example.com','$2y$10$57/tR0gDFwjVsoBCnm20NeLjvFdZgRxSs.TiQOFgaPbM1ikgReexS','mr','oseias','silveira','male','8514 rua vinte e um','divinópolis','são paulo','57384','0000-00-00','(58) 5758-0664','(04) 8420-1911','','','https://randomuser.me/api/portraits/men/32.jpg','https://randomuser.me/api/portraits/med/men/32.jpg','https://randomuser.me/api/portraits/thumb/men/32.jpg','BR','2016-08-18 22:30:21','2016-08-19 01:38:24'),(14,'bluepanda600','yentl.grooters@example.com','$2y$10$Fosl7jToIiw2et5LPMqct.OP/yYujrYFkTDQIrAnEv2ijKfPOf1sK','ms','yentl','grooters','female','6863 burgemeester reigerstraat','breda','noord-brabant','22177','0000-00-00','(583)-907-8567','(863)-602-3352','BSN','23604798','https://randomuser.me/api/portraits/women/75.jpg','https://randomuser.me/api/portraits/med/women/75.jpg','https://randomuser.me/api/portraits/thumb/women/75.jpg','NL','2016-08-18 22:30:21','2016-08-19 01:38:24'),(15,'goldenrabbit177','adam.olsen@example.com','$2y$10$qYprroRUgjDTJfrNGDCb.OUMTZA/s6WEvkEF96GPvFf/apjv9CuYK','mr','adam','olsen','male','4348 hadsundvej','randers nø','sjælland','12531','2010-05-19','42928539','17475528','CPR','093010-7395','https://randomuser.me/api/portraits/men/78.jpg','https://randomuser.me/api/portraits/med/men/78.jpg','https://randomuser.me/api/portraits/thumb/men/78.jpg','DK','2016-08-18 22:30:21','2016-08-19 01:38:24'),(16,'silverbear255','juho.latvala@example.com','$2y$10$/mkkzF8mHigtjBij761erO9R/o/LK7EGhZqzefPJ9.Y8xnpx8EpNK','mr','juho','latvala','male','9855 visiokatu','tervola','north karelia','53585','0000-00-00','08-126-665','048-086-35-37','HETU','92565926-Q','https://randomuser.me/api/portraits/men/53.jpg','https://randomuser.me/api/portraits/med/men/53.jpg','https://randomuser.me/api/portraits/thumb/men/53.jpg','FI','2016-08-18 22:30:22','2016-08-19 01:38:24'),(17,'lazymeercat759','ruby.lee@example.com','$2y$10$caDh949s.uZUq2.CK0xXyuo/S0CVJo/7xzk38TogX4UminzMZVI2i','miss','ruby','lee','female','6678 saint aubyn street','nelson','west coast','47965','0000-00-00','(340)-629-4984','(192)-206-0975','','','https://randomuser.me/api/portraits/women/93.jpg','https://randomuser.me/api/portraits/med/women/93.jpg','https://randomuser.me/api/portraits/thumb/women/93.jpg','NZ','2016-08-18 22:30:22','2016-08-19 01:38:24'),(18,'bluemeercat251','elizabeth.roy@example.com','$2y$10$lDGLtnnxHb5CltvMpF1V1.XuycmVcZ9kz/Z3O9AT1tWDOGQkRGULW','mrs','elizabeth','roy','female','7481 concession road 23','south river','nunavut','11376','0000-00-00','513-885-5113','329-088-5041','','','https://randomuser.me/api/portraits/women/82.jpg','https://randomuser.me/api/portraits/med/women/82.jpg','https://randomuser.me/api/portraits/thumb/women/82.jpg','CA','2016-08-18 22:30:22','2016-08-19 01:38:24'),(19,'smalltiger504','millie.roberts@example.com','$2y$10$QbztJ4twNSZRFLUI8zUel.DXLsoZIkmGCGRqL3e28IjU1mHJejzTq','ms','millie','roberts','female','9676 nelson quay','palmerston north','west coast','34128','0000-00-00','(346)-470-4456','(713)-802-4944','','','https://randomuser.me/api/portraits/women/16.jpg','https://randomuser.me/api/portraits/med/women/16.jpg','https://randomuser.me/api/portraits/thumb/women/16.jpg','NZ','2016-08-18 22:30:22','2016-08-19 01:38:24'),(20,'goldenfish388','kaya.tuğlu@example.com','$2y$10$lmowXkxMz3zmbRMvSM97vuZfX5Z3MD4qrIvq.UBNj0L1ISyqABQ.6','mr','kaya','tuğlu','male','1178 filistin cd','mardin','kayseri','16539','0000-00-00','(631)-582-7927','(455)-407-9032','','','https://randomuser.me/api/portraits/men/79.jpg','https://randomuser.me/api/portraits/med/men/79.jpg','https://randomuser.me/api/portraits/thumb/men/79.jpg','TR','2016-08-18 22:30:22','2016-08-19 01:38:24'),(21,'greentiger593','pedro.vega@example.com','$2y$10$fDODYNexeWEiMijLRWYsAeGqS77PWhE1ypDHLrCChmy/buPzzGAJu','mr','pedro','vega','male','7669 avenida del planetario','zaragoza','galicia','66660','0000-00-00','960-522-181','611-524-378','DNI','55248319-B','https://randomuser.me/api/portraits/men/43.jpg','https://randomuser.me/api/portraits/med/men/43.jpg','https://randomuser.me/api/portraits/thumb/men/43.jpg','ES','2016-08-18 22:30:22','2016-08-19 01:38:24');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_08_18_214217_create_customers_table',1),('2016_08_18_214334_create_roles_and_permissions_tables',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'delete-customers','Can delete customers','2016-08-19 01:04:25','2016-08-19 01:04:25');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(6,1);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super-admin','Super Administrator','2016-08-19 00:57:00','2016-08-19 00:57:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'George Georgaras','ggeorgaras@gmail.com','$2y$10$rDxaw7hQeqRnawri8S0.JOQJNEHXS74A2CvCCZ.JaTaoVRHN.wT4C','9Fu2c0CVQI9PjwaUzBps9YLQ4jSWA0BgkAsEhlpbqZjUQ1pp03VX4lxoaHDO','JrCieP5fapW3drfvDCJ2WEOsyPByGmxHqvbLbi0rTTkpd3yJMV54aXPePTYq','2016-08-18 20:30:40','2013-12-05 19:02:07'),(2,'Administrator','admin@example.com','$2y$10$gP2nKd2Jp.HDDCSSyMXOc.EnyE.xAFvoENTNodJkKbszqVxqlr0W.','JQzVULsEtGOS7g18mHvaRIWi3bhCQxMb6pg75ncUs1Ld5llTHHMxHCjyWxCp',NULL,'2016-08-19 01:09:49','2016-08-19 01:09:49'),(6,'SuperAdministrator','super@example.com','$2y$10$d6nsOB49H0el/4PrEQPkru37cbIiIJ8ewX4lrWEi22XSAQUS6PJrq','UXOwVHpc9yaYp8XGUj2bEI5wZNHMHN1fwBwPN6R2pXl9ApcowqdOMgwOkbEW','F5kXYw9167MBMEZKzWun1EcDZOAbcFEuf78l6s9Ku4WK9VSwoPVsPsbOrQJf','2016-08-19 01:11:40','2016-08-19 01:38:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-19 12:18:28
